#!/usr/bin/env python3
import hashlib
import os
import sys
import cgi
import cgitb
import codecs
import html
import requests
import http.cookies
from uuid import uuid4

import psycopg2
from psycopg2 import sql

from user import User

cgitb.enable()

sys.stdout = codecs.getwriter("utf-8")(sys.stdout.detach())
try:
    cookies = http.cookies.SimpleCookie(os.environ['HTTP_COOKIE'])
    session_id = cookies.get('session_id')
    username = cookies.get('username')
except KeyError:
    session_id = None
    username = None
if session_id is None:
    print(f'Set-cookie: session_id={uuid4()}; expires=Wed May 18 03:33:20 2033')
if username is not None:
    print("Content-type: text/html")
    print(f'''
        <!DOCTYPE html>
        <html lang="ru">
            <head>
                <title>Авторизация</title>
                <meta charset="UTF-8">
                <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
            </head>
            <body>
                <h1>{username.value}, Вы уже авторизованы</h1>
                <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
            </body>
        </html>
        ''')
    exit()

f = False
form = cgi.FieldStorage()
if form.getvalue('field-username'):
    username0 = html.escape(form.getvalue('field-username'))
else:
    f = True
if form.getvalue('field-password'):
    password = html.escape(form.getvalue('field-password'))
else:
    f = True

if not f:
    user = User(username0, password)
    conn = psycopg2.connect(
        dbname='postgres',
        user='myuser',
        password='UigfjL0f',
        host='localhost'
    )
    with conn.cursor() as cursor:
        conn.autocommit = True
        try:
            insert = f"SELECT * FROM users WHERE username = '{username0}'"
            cursor.execute(insert)
            record = cursor.fetchall()
            st = str(record[0][2])
            st.rstrip()
            pasp = user.check_hash(st)
        except (psycopg2.Error, IndexError) as er:
            f = True
    conn.close()


if f or not pasp:

    print("Content-type: text/html")
    print()
    print(f'''
    <!DOCTYPE html>
    <html lang="ru">
        <head>
            <title>Авторизация</title>
            <meta charset="UTF-8">
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
        </head>
        <body>
            <h1>Введены неверные логин или пароль!</h1>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
        </body>
    </html>
    ''')

else:
    print(f'Set-cookie: username={username0}; expires=Wed May 18 03:33:20 2033')
    print(f'Set-cookie: field-name-1={username0}; expires=Wed May 18 03:33:20 2033')
    print("Content-type: text/html")
    print(f'''
    <!DOCTYPE html>
    <html lang="ru">
        <head>
            <title>Авторизация</title>
            <meta charset="UTF-8">
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
        </head>
        <body>
            <h1>Здравствуйте, {username0}</h1>
            <p>Перейти в свой профиль</p>
            <a href="profile.py" class="btn btn-danger">Профиль</a>
            <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
        </body>
    </html>
    ''')
