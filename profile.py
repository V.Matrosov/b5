#!/usr/bin/env python3

import os
import sys
import cgi
import cgitb
import codecs
import html
import http.cookies

import psycopg2
from psycopg2 import sql

sys.stdout = codecs.getwriter("utf-8")(sys.stdout.detach())

cookies = http.cookies.SimpleCookie(os.environ['HTTP_COOKIE'])
username = cookies.get('username')
f = False
form = None

conn = psycopg2.connect(
    dbname='postgres',
    user='myuser',
    password='UigfjL0f',
    host='localhost'
)
with conn.cursor() as cursor:
    conn.autocommit = True
    try:
        insert = f"SELECT * FROM form WHERE first_name = '{username.value}'"
        cursor.execute(insert)
        form = cursor.fetchall()
    except psycopg2.Error:
        f = True
        form = None
conn.close()

print("Content-type: text/html")
print()
print(f'''
    <!DOCTYPE html>
        <html lang="ru">
            <head>
                <meta charset="UTF-8">
                <title>Профиль</title>
                <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
            </head>
            <body>
''')
try:
    if form is not None and not f:
        print(f'''
            <h1>{username.value}, Ваши ответы на форму</h1>
            <div class="row">
                <div class="col-6">
                    Имя: 
                </div>
                <div class="col-6">
                    {form[0][1]}
                </div>
            </div>
            <div class="row">
                <div class="col-6">
                    Электронная почта:
                </div>
                <div class="col-6">
                    {form[0][2]}
                </div>
            </div>
            <div class="row">
                <div class="col-6">
                    Дата отправки:
                </div>
                <div class="col-6">
                    {str(form[0][3])}
                </div>
            </div>
            <div class="row">
                <div class="col-6">
                    Пол:
                </div>
                <div class="col-6">
                    {form[0][4]}
                </div>
            </div>
            <div class="row">
                <div class="col-6">
                    Количество конечностей:
                </div>
                <div class="col-6">
                    {form[0][5]}
                </div>
            </div>
            <div class="row">
                <div class="col-6">
                    Сверхспособности:
                </div>
                <div class="col-6">
                    {form[0][6]}
                </div>
            </div>
            <div class="row">
                <div class="col-6">
                    Биография:
                </div>
                <div class="col-6">
                    {form[0][7]}
                </div>
            </div>
            <a href="/backend-5/change_form.html" class="btn btn-primary">Изменить ответы</a>
        ''')

    elif username is not None:
        print(f'''
        <h1>{username.value}, Вы ещё не заполняли форму</h1>
        <a href="new_form.py" class="btn btn-primary">Заполнить форму</a>
        ''')
    else:
        print(f'''
            <h1>Вам необходимо авторизоваться</h1>
            <a href="/backend-5/login.html" class="btn btn-warning">Авторизация</a>
            ''')
    print('''    
        </body>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
    </html>
    ''')
except IndexError:
    print(f'''
            <h1>{username.value}, Вы ещё не заполняли форму</h1>
            <a href="new_form.py" class="btn btn-primary">Заполнить форму</a>
            </body>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
    </html>
    ''')
