import hashlib
from _generator import get_random_password, get_random_username
from itertools import count


class User:
    _id = count(0)

    def __init__(self, username, password):
        self.username = username
        self.password = hashlib.sha256(password.encode('utf-8'))
        self.id = next(self._id)

    def get_username(self):
        return self.username

    def get_password(self):
        return self.password.hexdigest()

    def get_id(self):
        return self._id

    def __str__(self):
        return "%s %s"%(self.username, self.password.hexdigest())

    def update_user(self, username, password):
        self.username = username
        self.password = hashlib.sha256(password.encode('utf-8'))

    def set_username(self, username):
        self.username = username

    def set_password(self, password):
        self.password = hashlib.sha256(password.encode('utf-8'))

    def check_password(self, somestr):
        return hashlib.sha256(somestr.encode('utf-8')).hexdigest() == self.password.hexdigest()

    def check_hash(self, hash):
        for i in range(64):
            if self.password.hexdigest()[i] != hash[i]:
                return False
        return True


    @staticmethod
    def generate_user():
        password = get_random_password()
        new_user = User(get_random_username(), password)
        return [new_user, password]
